import Axios from "axios"
import { GET, POST  } from "./postActionTypes"

export const get = () => {
    return async dp => {
      const result = await Axios.get('https://jsonplaceholder.typicode.com/posts')
      dp({
        type: GET,
        data: result.data
      })
    }
  }

  export const post = () => {
    return async dp => {
      const result = await Axios.post('https://jsonplaceholder.typicode.com/posts')
      dp({
        type: POST,
        data: result.data
      })
    }
  }
  
  